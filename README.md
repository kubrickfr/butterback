# Butterback

[![doc](https://img.shields.io/badge/doc-public-blue)](https://rubdos.gitlab.io/butterback/doc/butterback/)
[![doc-private](https://img.shields.io/badge/doc-dev-blue)](https://rubdos.gitlab.io/butterback/doc-private/butterback/)

*... because you want your `btrfs` subvolume back.*

Butterback is a `btrfs` subvolume back-up tool for public cloud.
It encrypts a subvolume and streams it to Amazon Glacier.

It's very much a work-in-progress at this point, although I'd love to get some feedback!
In its current form, it is not a valuable disaster recovery tool.
Don't use it, yet!

## Systemd

The source code ships a systemd oneshot unit and timer, both parametrized in a subvolume.
They hardcode a back-up frequency, vault and AWS region,
so customize to your liking.

You use them by:

```sh
cp systemd/* /etc/systemd/system/
UNIT=$(systemd-escape -p /var/lib/urbackup) # subvolume path to backup
systemd enable --now butterback@$UNIT.timer
```

This all assumes that your public key and inventory reside at respectively `/var/lib/butterback/butterback.pub` and `/var/lib/butterback/inventory.yaml`.
