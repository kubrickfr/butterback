use std::{
    fmt::Display,
    iter::FromIterator,
    path::{Path, PathBuf},
    str::FromStr,
};

use itertools::Itertools;

#[derive(Debug, serde::Serialize, serde::Deserialize, PartialEq, Eq, Clone)]
pub enum BackupJobKind {
    Full,
    Incremental { parent: String },
}

#[derive(Debug, Default, serde::Serialize, serde::Deserialize, PartialEq, Eq)]
pub struct Inventory {
    inventory: Vec<InventoryEntry>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize, PartialEq, Eq, Clone)]
pub struct InventoryEntry {
    pub archive: ArchiveDescription,
    pub job: JobDescription,
}

impl Ord for InventoryEntry {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        (&self.job.volume, &self.archive.date).cmp(&(&other.job.volume, &other.archive.date))
    }
}

impl PartialOrd for InventoryEntry {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl FromIterator<InventoryEntry> for Inventory {
    fn from_iter<T: IntoIterator<Item = InventoryEntry>>(iter: T) -> Self {
        let mut inventory = Inventory {
            inventory: iter.into_iter().collect(),
        };
        inventory.inventory.sort();
        inventory
    }
}

impl Inventory {
    pub fn write(&self, path: &Path) -> anyhow::Result<()> {
        let f = std::fs::File::create(path)?;
        serde_yaml::to_writer(f, self)?;
        Ok(())
    }

    pub fn is_empty(&self) -> bool {
        self.inventory.is_empty()
    }

    pub fn load(path: &Path) -> anyhow::Result<Self> {
        let f = std::fs::File::open(path)?;
        let mut s: Self = serde_yaml::from_reader(f)?;
        s.inventory.sort();
        Ok(s)
    }

    pub fn volumes(&self) -> impl Iterator<Item = PathBuf> + '_ {
        self.inventory
            // #[feature(slice_group_by)] #80552 ?
            .iter()
            .map(|entry| PathBuf::from(&entry.job.volume))
            .unique()
    }

    pub fn push(&mut self, desc: ArchiveDescription) -> anyhow::Result<()> {
        let job_desc: JobDescription = desc.description.parse()?;
        let entry = InventoryEntry {
            archive: desc,
            job: job_desc,
        };
        match self.inventory.binary_search(&entry) {
            Ok(_i) => anyhow::bail!("Duplicate inventory entry"),
            Err(i) => {
                self.inventory.insert(i, entry);
            }
        }
        Ok(())
    }

    pub fn backups_for<'s>(
        &'s self,
        subvolume: &'s Path,
    ) -> impl Iterator<Item = &InventoryEntry> + 's {
        self.inventory
            .iter()
            .filter(move |entry| Path::new(&entry.job.volume) == subvolume)
    }

    pub fn entry_for_snapshot(&self, snapshot: &Path) -> Option<&InventoryEntry> {
        self.inventory
            .iter()
            .find(move |entry| Path::new(&entry.job.local_snapshot) == snapshot)
    }
}

#[derive(Debug, serde::Serialize, serde::Deserialize, PartialEq, Eq, Clone)]
pub struct ArchiveDescription {
    pub archive_id: String,
    pub description: String,
    pub date: chrono::DateTime<chrono::Utc>,
    pub metadata: String,
}

#[derive(Debug, serde::Serialize, serde::Deserialize, PartialEq, Eq, Clone)]
pub struct JobDescription {
    pub volume: String,
    pub local_snapshot: String,
    pub uuid: String,
    pub kind: BackupJobKind,
    pub generation: u64,
    pub subvolumes: Vec<String>,
}

impl FromStr for JobDescription {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        const SEP: &str = "JSON::";
        let start_pos = s
            .find(SEP)
            .ok_or_else(|| anyhow::format_err!("No --- separator in description"))?;
        let (_preamble, yaml) = s.split_at(start_pos + SEP.len());
        Ok(serde_yaml::from_str(yaml)?)
    }
}

impl Display for JobDescription {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Butterback backup - JSON::")?;
        let s = serde_json::to_string(self).map_err(|_e| std::fmt::Error)?;
        f.write_str(&s)?;

        Ok(())
    }
}

#[async_trait::async_trait]
pub trait CloudTarget {
    type UploadJob;
    type RetrievalJob;
    type RetrievalParameters: Default;

    async fn initiate_upload_job(
        &self,
        description: JobDescription,
    ) -> anyhow::Result<Self::UploadJob>;

    async fn inventory(&self) -> anyhow::Result<Inventory>;

    async fn continue_job(
        &self,
        _archive: &ArchiveDescription,
    ) -> anyhow::Result<Option<Self::RetrievalJob>> {
        Ok(None)
    }
    async fn initiate_retrieval_job(
        &self,
        archive: &ArchiveDescription,
        params: Self::RetrievalParameters,
    ) -> anyhow::Result<Self::RetrievalJob>;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn ser_de_description() {
        let full_descr = JobDescription {
            kind: BackupJobKind::Full,
            volume: "/foo/bar".into(),
            local_snapshot: "/foo/bar/.butterback/aoeu".into(),
            uuid: "bar".into(),
            generation: 3,
            subvolumes: vec!["/foo/bar/coq".into(), "/foo/bar/uwu".into()],
        };
        let incr_descr = JobDescription {
            kind: BackupJobKind::Incremental {
                parent: "Some Id from Amazon".into(),
            },
            volume: "/foo/bar".into(),
            local_snapshot: "/foo/bar/.butterback/aoeu".into(),
            uuid: "bar".into(),
            generation: 3,
            subvolumes: vec!["/foo/bar/coq".into()],
        };

        for item in &[full_descr, incr_descr] {
            let s = item.to_string();
            let deser: JobDescription = s.parse().unwrap();
            assert_eq!(&deser, item);
        }
    }
}
