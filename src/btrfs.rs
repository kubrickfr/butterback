use futures::stream::FuturesUnordered;
use futures::TryStreamExt;
use indicatif::HumanBytes;
use std::collections::HashMap;
use std::collections::VecDeque;
use std::path::{Path, PathBuf};
use std::pin::Pin;
use std::process::ExitStatus;
use std::process::Stdio;
use std::task::Poll;
use tokio::io::{AsyncRead, AsyncWrite};
use tokio::process::{Child, ChildStdin, ChildStdout, Command};

use crate::backup::{Inventory, InventoryEntry};

#[derive(thiserror::Error, Debug)]
pub enum BtrfsError {
    #[error("IO error")]
    IoError(#[from] std::io::Error),
    #[error("Unknown btrfs read error (btrfs return status {0})")]
    Unknown(ExitStatus),
    #[error("Supplied argument is not a readable btrfs subvolume (missing permissions?)")]
    NotASubvolume(PathBuf),
    #[error("Invalid snapshots path: could not create subvolume")]
    CannotCreateSubvolume,
    #[error("Could not create snapshot")]
    SnapshotFailed,
}

pub struct BtrfsBackupPlanner {
    root: BtrfsSubvolumeMetadata,
    btrfs_prefix: String,
}

#[derive(Clone, Debug)]
pub struct BtrfsSubvolumeMetadata {
    pub subvolume: PathBuf,
    pub name: String,
    pub generation: u64,
    pub uuid: String,
    pub parent_uuid: Option<String>,

    pub usage_referenced: Option<u64>,
    pub usage_exclusive: Option<u64>,
}

impl BtrfsSubvolumeMetadata {
    pub async fn parse(subvolume: PathBuf) -> Result<BtrfsSubvolumeMetadata, BtrfsError> {
        let result = Command::new("btrfs")
            .arg("subvolume")
            .arg("show")
            .arg("-b")
            .arg(&subvolume)
            .output()
            .await?;
        if !result.status.success() {
            return Err(BtrfsError::NotASubvolume(subvolume));
        }

        let field = |name: &str| -> Option<String> {
            let expr = r"(?m)^\s*".to_string() + name + r":\s*(\S+)\s*$";
            let parse = regex::Regex::new(&expr).unwrap();
            let captures = parse
                .captures(std::str::from_utf8(&result.stdout).expect("UTF-8 output from btrfs"))?;
            Some(captures[1].to_string())
        };

        let parent_uuid = field("Parent UUID").expect("Parent UUID in output");
        let parent_uuid = if parent_uuid == "-" {
            None
        } else {
            Some(parent_uuid)
        };

        use std::str::FromStr;
        Ok(BtrfsSubvolumeMetadata {
            subvolume,
            name: field("Name").expect("name in output"),
            generation: field("Generation")
                .expect("generation in output")
                .parse()
                .unwrap(),
            uuid: field("UUID").expect("uuid in output"),
            parent_uuid,

            usage_referenced: field("Usage referenced")
                .as_deref()
                .map(u64::from_str)
                .transpose()
                .expect("parsable usage (ref)"),
            usage_exclusive: field("Usage referenced")
                .as_deref()
                .map(u64::from_str)
                .transpose()
                .expect("parsable usage (excl)"),
        })
    }
}

impl BtrfsBackupPlanner {
    pub async fn for_subvolume(subvolume: PathBuf) -> Result<BtrfsBackupPlanner, BtrfsError> {
        let meta = BtrfsSubvolumeMetadata::parse(subvolume).await?;
        let btrfs_prefix = meta.name.clone();
        log::info!("BTRFS volume name: `{}`", btrfs_prefix);

        Ok(Self {
            root: meta,
            btrfs_prefix,
        })
    }

    pub async fn list_subvolumes(self) -> Result<BtrfsSubvolumeList, BtrfsError> {
        let mut to_snapshot = vec![];
        let mut to_recurse = VecDeque::from(vec![self]);

        while let Some(Self { root, btrfs_prefix }) = to_recurse.pop_front() {
            let subvol = root.subvolume.clone();

            to_snapshot.push(root.clone());
            // Get all recursive subvolumes
            let result = Command::new("btrfs")
                .arg("subvolume")
                .arg("list")
                .arg("-o") // only at this path
                .arg(&subvol)
                .output()
                .await?;
            if !result.status.success() {
                log::error!("BTRFS error {}", String::from_utf8_lossy(&result.stdout));
                return Err(BtrfsError::Unknown(result.status));
            }
            // ID 23145 gen 640966 top level 20106 path peertube-storage/.snapshot-20210611_
            let parser = regex::Regex::new(
            r"^\s*ID\s+(?P<id>\d+)\s+gen\s+(?P<gen>\d+)\s+top level\s+(?P<toplevel>\d+)\s+path\s+(?P<path>\S+)\s*$",
        )
        .unwrap();
            for line in std::str::from_utf8(&result.stdout).expect("UTF-8").lines() {
                log::trace!("BTRFS output {}", line);
                let cap = parser.captures(&line).expect("btrfs output");
                let sub = cap.name("path").unwrap().as_str();
                let path = Path::new(sub);
                let path = path.strip_prefix(&btrfs_prefix).expect("subvolume prefix");
                let sub = subvol.join(path);
                log::info!("Recursively found subvolume: {}", sub.display());

                let mut planner = Self::for_subvolume(sub.to_owned()).await?;
                planner.btrfs_prefix = btrfs_prefix.to_string() + "/" + &planner.btrfs_prefix;
                to_recurse.push_back(planner);
            }
        }
        // btrfs subvolume list -ro /var/www/peertube/storage/
        Ok(BtrfsSubvolumeList { to_snapshot })
    }
}

pub struct BtrfsSubvolumeList {
    /// A vector of subvolumes at PathBuf that should be recursively pseudo-snapshotted.
    to_snapshot: Vec<BtrfsSubvolumeMetadata>,
}

impl BtrfsSubvolumeList {
    /// Exclude certain path prefix from recursive cloning.
    ///
    /// This is useful for instance to exclude the `.butterback` snapshots.
    pub fn exclude(&mut self, prefix: &Path) -> bool {
        let l = self.to_snapshot.len();
        self.to_snapshot
            .retain(|x| !x.subvolume.starts_with(prefix));
        l != self.to_snapshot.len()
    }

    pub fn select_strategy_for(
        meta: &BtrfsSubvolumeMetadata,
        inventory: &Inventory,
    ) -> Result<SubvolumeStrategy, BtrfsError> {
        let subvolume = &meta.subvolume;
        // Use incremental if:
        // - The subvolume has a parent
        // - We still locally have the parent
        // - There are changes since the last backup (generation check)
        // Use full if:
        // - There are changes since the last backup (generation check/first backup)
        // Else, don't backup
        let mut existing: Vec<_> = inventory.backups_for(subvolume).collect();
        log::trace!("Considering {:?} for {}", existing, subvolume.display());
        while let Some(entry) = existing.pop() {
            if entry.job.uuid != meta.uuid {
                // Between remote and now, this subvolume has been replaced by another.  The delta
                // is probably completely unrelated, so we don't do incremental here.
                continue;
            }
            if meta.generation == entry.job.generation {
                return Ok(SubvolumeStrategy::UpToDate(entry.clone()));
            } else {
                let local_snapshot = Path::new(&entry.job.local_snapshot);
                // Only do incremental if we can actually compute the delta.
                if local_snapshot.exists() {
                    return Ok(SubvolumeStrategy::Incremental((*entry).clone()));
                }
            }
        }
        // No prior/available backups for this volume, do a full backup.
        Ok(SubvolumeStrategy::Full)
    }

    pub fn compute_plan(self, inventory: &Inventory) -> Result<BtrfsBackupPlan, BtrfsError> {
        let mut plan: Vec<(BtrfsSubvolumeMetadata, SubvolumeStrategy)> = vec![];
        let snapshot_order = self.to_snapshot.clone();

        // Now we compute a topological sort, based on uuid parents.
        let mut topological = vec![];
        {
            let mut s: Vec<_> = self
                .to_snapshot
                .iter()
                .filter(|x| x.parent_uuid.is_none())
                .collect();
            while let Some(x) = s.pop() {
                topological.push(x);
                // Now we can insert everyone with `x` as parent
                for y in self
                    .to_snapshot
                    .iter()
                    .filter(|x| x.parent_uuid.as_ref() == Some(&x.uuid))
                {
                    topological.push(y);
                }
            }
            // Finally we append all subvolumes that have parents outside of the backed up tree.
            for target in &self.to_snapshot {
                if topological.iter().find(|x| x.uuid == target.uuid).is_none() {
                    topological.push(target);
                }
            }
        };
        // Check that all the volumes are present in both.
        assert_eq!(topological.len(), self.to_snapshot.len());

        // Compute parents for incremental backup
        for subvolume in topological {
            let mut strategy = Self::select_strategy_for(&subvolume, inventory)?;
            // Try to find a delta
            if strategy == SubvolumeStrategy::Full {
                if let Some(parent_id) = subvolume.parent_uuid.as_deref() {
                    if let Some((parent, parent_strategy)) =
                        plan.iter().find(|(x, _)| x.uuid == parent_id)
                    {
                        strategy = if let SubvolumeStrategy::UpToDate(ref entry) = parent_strategy {
                            SubvolumeStrategy::Incremental(entry.clone())
                        } else {
                            // Full / Delta / Incremental will all create a new archive, postpone its resolution.
                            SubvolumeStrategy::Delta(parent.subvolume.clone())
                        };
                    }
                }
            }
            plan.push((subvolume.clone(), strategy));
        }
        Ok(BtrfsBackupPlan {
            snapshot_order,
            plan,
        })
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum SubvolumeStrategy {
    UpToDate(InventoryEntry),
    Full,
    /// Remote parent volume
    Incremental(InventoryEntry),
    /// Local parent volume that will be uploaded preceeding this volume
    // XXX Delta could also be based on *received uuid*.
    Delta(PathBuf),
}

/// Ordered combinations of Paths and upload strategies.
pub struct BtrfsBackupPlan {
    /// Ordered by how the snapshots should be taken (depth-first for FS).
    snapshot_order: Vec<BtrfsSubvolumeMetadata>,
    /// Ordered by how the snapshots should be uploaded  (top-down for parent-child snapshots)
    plan: Vec<(BtrfsSubvolumeMetadata, SubvolumeStrategy)>,
}

impl std::fmt::Display for BtrfsBackupPlan {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(
            f,
            "Backup plan with {} subvolumes",
            self.snapshot_order.len()
        )?;
        let mut up_to_date = 0;
        let mut incremental = 0;
        let mut delta = 0;
        let mut full = 0;
        for (vol, strat) in &self.plan {
            let desc = match strat {
                SubvolumeStrategy::Full => {
                    full += 1;
                    format!(
                        "full, {}",
                        vol.usage_referenced
                            .map(HumanBytes)
                            .map(|x| x.to_string())
                            .unwrap_or_else(|| "no estimate".into())
                    )
                }
                SubvolumeStrategy::UpToDate(entry) => {
                    up_to_date += 1;
                    format!("up to date, archive {}..", &entry.archive.archive_id[..16])
                }
                SubvolumeStrategy::Incremental(entry) => {
                    incremental += 1;
                    format!(
                        "incremental, based on {}.. ({})",
                        &entry.archive.archive_id[..16],
                        vol.usage_exclusive
                            .map(HumanBytes)
                            .map(|x| x.to_string())
                            .unwrap_or_else(|| "no estimate".into())
                    )
                }
                SubvolumeStrategy::Delta(subv) => {
                    delta += 1;
                    format!(
                        "delta, based on {}, {}",
                        subv.display(),
                        vol.usage_exclusive
                            .map(HumanBytes)
                            .map(|x| x.to_string())
                            .unwrap_or_else(|| "no estimate".into())
                    )
                }
            };
            writeln!(f, "├── {} ({})", vol.subvolume.display(), desc)?;
        }
        writeln!(
            f,
            "└────> Full backups: {}, incremental: {}, delta: {}, up-to-date: {}",
            full, incremental, delta, up_to_date
        )?;
        Ok(())
    }
}

impl BtrfsBackupPlan {
    pub async fn create_snapshots(
        self,
        target: &Path,
    ) -> Result<
        (
            HashMap<PathBuf, PathBuf>,
            Vec<(BtrfsSubvolumeMetadata, SubvolumeStrategy)>,
        ),
        BtrfsError,
    > {
        // First, create the `.butterback` directory
        if !target.exists() {
            log::trace!("Trying to create subvolume {}", target.display());
            let result = Command::new("btrfs")
                .arg("subvolume")
                .arg("create")
                .arg(target)
                .status()
                .await?;
            if !result.success() {
                return Err(BtrfsError::CannotCreateSubvolume);
            }
        }

        // Then, create all snapshots.
        let mut snapshots = HashMap::new();
        let mut snapshot_commands = FuturesUnordered::new();
        let time_suffix = chrono::Utc::now().to_rfc3339();
        for (i, source) in self.snapshot_order.into_iter().enumerate() {
            let name = source
                .subvolume
                .file_name()
                .map(|x| x.to_str().expect("utf-8 subvolume name"))
                .unwrap_or("root");
            let target = target.join(format!("{}-{}-{}", i, name, time_suffix));

            snapshot_commands.push(
                Command::new("btrfs")
                    .arg("subvolume")
                    .arg("snapshot")
                    .arg("-r")
                    .arg(&source.subvolume)
                    .arg(&target)
                    .status(),
            );
            log::debug!(
                "Snapshotting {} into {}",
                source.subvolume.display(),
                target.display()
            );
            snapshots.insert(source.subvolume, target);
        }
        while let Some(r) = snapshot_commands.try_next().await? {
            if !r.success() {
                return Err(BtrfsError::SnapshotFailed);
            }
        }

        Ok((snapshots, self.plan))
    }
}

pub struct BtrfsSubvolumeReadJob {
    btrfs_send_command: Child,
    btrfs_send_stdout: ChildStdout,
}

impl BtrfsSubvolumeReadJob {
    /// Create a BtrfsSubvolumeReadJob from a read-only subvolume.
    pub fn from_snapshot(subvolume: &Path) -> Result<Self, BtrfsError> {
        let mut cmd = Command::new("btrfs")
            .arg("send")
            .arg("-q") // quiet
            .stdout(Stdio::piped())
            .arg(subvolume)
            .spawn()?;

        let stdout = cmd.stdout.take().expect("piped stdout");

        Ok(Self {
            btrfs_send_command: cmd,
            btrfs_send_stdout: stdout,
        })
    }

    /// Create a BtrfsSubvolumeReadJob from a read-only subvolume.
    pub fn from_delta_snapshots(subvolume: &Path, parent: &Path) -> Result<Self, BtrfsError> {
        let mut cmd = Command::new("btrfs")
            .arg("send")
            .arg("-q") // quiet
            .arg("-p") // quiet
            .arg(parent) // quiet
            .arg(subvolume)
            .stdout(Stdio::piped())
            .spawn()?;

        let stdout = cmd.stdout.take().expect("piped stdout");

        Ok(Self {
            btrfs_send_command: cmd,
            btrfs_send_stdout: stdout,
        })
    }
}

impl AsyncRead for BtrfsSubvolumeReadJob {
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &mut tokio::io::ReadBuf<'_>,
    ) -> Poll<std::io::Result<()>> {
        if let Poll::Ready(Err(e)) = self.poll_ok(cx) {
            return Poll::Ready(Err(std::io::Error::new(std::io::ErrorKind::BrokenPipe, e)));
        }
        AsyncRead::poll_read(Pin::new(&mut self.btrfs_send_stdout), cx, buf)
    }
}

impl BtrfsSubvolumeReadJob {
    fn poll_ok(&mut self, _ctx: &mut std::task::Context<'_>) -> Poll<anyhow::Result<()>> {
        match self.btrfs_send_command.try_wait() {
            Ok(Some(status)) if !status.success() => {
                // Process exited. Badly.
                Poll::Ready(Err(BtrfsError::Unknown(status).into()))
            }
            Ok(Some(_status)) => Poll::Ready(Ok(())),
            Ok(None) => Poll::Pending,
            Err(e) => Poll::Ready(Err(e.into())),
        }
    }
}

impl Drop for BtrfsSubvolumeReadJob {
    fn drop(&mut self) {
        // Best effort kill
        self.btrfs_send_command.start_kill().ok();
    }
}

pub struct BtrfsSubvolumeWriteJob {
    btrfs_receive_command: Child,
    btrfs_receive_stdin: ChildStdin,
}

impl BtrfsSubvolumeWriteJob {
    /// Create a BtrfsSubvolumeWriteJob to a new subvolume.
    pub fn write_to_subvolume(subvolume: &Path) -> Result<Self, BtrfsError> {
        let mut cmd = Command::new("btrfs")
            .arg("receive")
            .arg("-q") // quiet
            .stdin(Stdio::piped())
            .arg(subvolume)
            .spawn()?;

        let stdin = cmd.stdin.take().expect("piped stdin");

        Ok(Self {
            btrfs_receive_command: cmd,
            btrfs_receive_stdin: stdin,
        })
    }
}

impl AsyncWrite for BtrfsSubvolumeWriteJob {
    fn poll_write(
        mut self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &[u8],
    ) -> Poll<Result<usize, std::io::Error>> {
        if let Poll::Ready(Err(e)) = self.poll_ok(cx) {
            return Poll::Ready(Err(std::io::Error::new(std::io::ErrorKind::BrokenPipe, e)));
        }
        Pin::new(&mut self.btrfs_receive_stdin).poll_write(cx, buf)
    }

    fn poll_flush(
        mut self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> Poll<Result<(), std::io::Error>> {
        if let Poll::Ready(Err(e)) = self.poll_ok(cx) {
            return Poll::Ready(Err(std::io::Error::new(std::io::ErrorKind::BrokenPipe, e)));
        }
        Pin::new(&mut self.btrfs_receive_stdin).poll_flush(cx)
    }

    fn poll_shutdown(
        mut self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> Poll<Result<(), std::io::Error>> {
        Pin::new(&mut self.btrfs_receive_stdin).poll_shutdown(cx)
    }
}

impl BtrfsSubvolumeWriteJob {
    fn poll_ok(&mut self, _ctx: &mut std::task::Context<'_>) -> Poll<anyhow::Result<()>> {
        match self.btrfs_receive_command.try_wait() {
            Ok(Some(status)) if !status.success() => {
                // Process exited. Badly.
                Poll::Ready(Err(BtrfsError::Unknown(status).into()))
            }
            Ok(Some(_status)) => Poll::Ready(Ok(())),
            Ok(None) => Poll::Pending,
            Err(e) => Poll::Ready(Err(e.into())),
        }
    }

    pub async fn close(mut self) -> anyhow::Result<()> {
        use tokio::io::AsyncWriteExt;
        self.flush().await?;
        drop(self.btrfs_receive_stdin);

        let status = self.btrfs_receive_command.wait().await?;
        if !status.success() {
            // Process exited. Badly.
            Err(BtrfsError::Unknown(status).into())
        } else {
            Ok(())
        }
    }
}

#[cfg(test)]
mod tests {}
