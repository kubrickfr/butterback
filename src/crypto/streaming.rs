use chacha20poly1305::{
    aead::{AeadInPlace, NewAead},
    ChaCha20Poly1305, Key, Nonce,
};
use tokio::io::AsyncRead;
use tokio::io::ReadBuf;

use super::{generate_keypair, PrivateKey, PublicKey};

pub const BLOCK_SIZE: usize = 1024 * 1024; // 1MB

pub struct BackupStreamSealer<T> {
    source: T,
    ephemeral_public: Option<PublicKey>,

    cipher: ChaCha20Poly1305,
    block: u64,
    // XXX zeroize
    scratch_space: Vec<u8>,
    filled_until: usize,

    written_to_sink: Option<usize>,
}
impl<T: AsyncRead> BackupStreamSealer<T> {
    pub fn from_source_and_key(source: T, key: &PublicKey) -> Self {
        let (ephemeral_public, ephemeral_private) = generate_keypair();

        let shared_secret = ephemeral_private.agree_with(key);

        let mut expanded_key = [0u8; 32];
        {
            use tiny_keccak::Hasher;
            let mut kdf = tiny_keccak::Shake::v256();
            kdf.update(b"butterback backup, and nothing up my sleeves.");
            kdf.update(&key.to_bytes());
            kdf.update(&ephemeral_public.to_bytes());
            kdf.update(&shared_secret);
            kdf.finalize(&mut expanded_key);
        }
        let cipher = ChaCha20Poly1305::new(Key::from_slice(&expanded_key));

        Self {
            source,
            ephemeral_public: Some(ephemeral_public),

            cipher,
            block: 0,
            scratch_space: vec![0u8; BLOCK_SIZE + 16],
            filled_until: 0,
            written_to_sink: None,
        }
    }

    pub fn get_ref(&self) -> &T {
        &self.source
    }
}

impl<T: AsyncRead> AsyncRead for BackupStreamSealer<T>
where
    Self: Unpin,
    T: Unpin,
{
    fn poll_read(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &mut ReadBuf<'_>,
    ) -> std::task::Poll<std::io::Result<()>> {
        if let Some(key) = self.ephemeral_public.take() {
            buf.put_slice(&key.to_bytes());
            log::trace!("Writing public key {:?}", &key.to_bytes());
            return std::task::Poll::Ready(Ok(()));
        }

        // Heh.  If I directly use &mut self.source, borrowcheck hates me.
        // Going through deref_mut() is apparently annoying.
        let Self {
            ref mut source,
            ref mut scratch_space,
            ref mut cipher,
            ref mut filled_until,
            ref mut block,
            ref mut written_to_sink,
            ..
        } = *self;

        // Need to create a new block first.
        if written_to_sink.is_none() {
            scratch_space.resize(BLOCK_SIZE, 0);
            let mut input_buf = ReadBuf::new(&mut scratch_space[..BLOCK_SIZE]);
            input_buf.advance(*filled_until);

            // As long as we didn't fill the block yet...
            while *filled_until != BLOCK_SIZE {
                //  ... try reading more
                let source = std::pin::Pin::new(&mut *source);
                futures::ready!(AsyncRead::poll_read(source, cx, &mut input_buf))?;
                // (the above will yield to Pending if no more bytes are available.)

                // ... if it didn't change, we're at the end of the stream.
                if *filled_until == input_buf.filled().len() {
                    // End of stream
                    break;
                }
                *filled_until = input_buf.filled().len();
            }

            if *filled_until == 0 {
                // EOF, return without touching the buffer.
                return std::task::Poll::Ready(Ok(()));
            }

            // Encrypt buffer
            let mut nonce = [0u8; 12];
            nonce[0..8].clone_from_slice(&block.to_le_bytes());
            scratch_space.truncate(*filled_until);
            cipher
                .encrypt_in_place(Nonce::from_slice(&nonce), b"", scratch_space)
                .expect("encrypt");
            *block += 1;
        }

        let written_to_sink = written_to_sink.get_or_insert(0);

        let amount_to_write =
            std::cmp::min(buf.remaining(), scratch_space.len() - *written_to_sink);

        assert!(buf.remaining() >= amount_to_write);
        buf.put_slice(&scratch_space[*written_to_sink..][..amount_to_write]);
        *written_to_sink += amount_to_write;

        // If everything is written out, advance.
        if *written_to_sink == scratch_space.len() {
            *filled_until = 0;
            self.written_to_sink = None;
        }

        std::task::Poll::Ready(Ok(()))
    }
}

pub struct BackupStreamOpener<T> {
    source: T,
    ephemeral_public: Option<PublicKey>,
    static_private: PrivateKey,

    cipher: Option<ChaCha20Poly1305>,
    block: u64,
    // XXX zeroize
    scratch_space: Vec<u8>,
    filled_until: usize,
    written_to_sink: Option<usize>,
}

impl<T> BackupStreamOpener<T> {
    pub fn from_source_and_key(source: T, key: PrivateKey) -> Self {
        Self {
            source,
            ephemeral_public: None,
            static_private: key,

            cipher: None,
            block: 0,
            scratch_space: Vec::with_capacity(BLOCK_SIZE + 16),
            filled_until: 0,
            written_to_sink: None,
        }
    }

    fn compute_shared_secret(&mut self) -> anyhow::Result<()> {
        let shared_secret = self
            .static_private
            .agree_with(self.ephemeral_public.as_ref().unwrap());

        let static_public = self.static_private.public();

        let mut expanded_key = [0u8; 32];
        {
            use tiny_keccak::Hasher;
            let mut kdf = tiny_keccak::Shake::v256();
            kdf.update(b"butterback backup, and nothing up my sleeves.");
            kdf.update(&static_public.to_bytes());
            kdf.update(&self.ephemeral_public.as_ref().unwrap().to_bytes());
            kdf.update(&shared_secret);
            kdf.finalize(&mut expanded_key);
        }
        self.cipher = Some(ChaCha20Poly1305::new(Key::from_slice(&expanded_key)));
        Ok(())
    }
}

impl<T: AsyncRead> AsyncRead for BackupStreamOpener<T>
where
    Self: Unpin,
    T: Unpin,
{
    fn poll_read(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &mut ReadBuf<'_>,
    ) -> std::task::Poll<Result<(), std::io::Error>> {
        if self.ephemeral_public.is_none() {
            let mut pubk = [0u8; 32];
            let mut input_buf = ReadBuf::new(&mut pubk);
            futures::ready!(std::pin::Pin::new(&mut self.source).poll_read(cx, &mut input_buf))?;
            assert!(input_buf.filled().len() == 32, "Can't partially read pubk");
            // Read 32 bytes and do the key ex
            log::trace!("Decoding public key {:?}", pubk);
            self.ephemeral_public = Some(PublicKey::from_bytes(pubk).map_err(|e| {
                let e = anyhow::format_err!("Invalid ephemeral public key ({})", e);
                std::io::Error::new(std::io::ErrorKind::InvalidData, e)
            })?);
            self.compute_shared_secret()
                .map_err(|e| std::io::Error::new(std::io::ErrorKind::InvalidData, e))?;
        }

        let Self {
            ref mut source,
            ref mut scratch_space,
            ref mut cipher,
            ref mut filled_until,
            ref mut block,
            ref mut written_to_sink,
            ..
        } = *self;

        // Read a new block first
        if written_to_sink.is_none() {
            scratch_space.resize(BLOCK_SIZE + 16, 0);
            let mut input_buf = ReadBuf::new(&mut scratch_space[..(BLOCK_SIZE + 16)]);
            input_buf.advance(*filled_until);

            // As long as we didn't fill the block yet...
            while *filled_until != BLOCK_SIZE + 16 {
                //  ... try reading more
                let source = std::pin::Pin::new(&mut *source);
                futures::ready!(AsyncRead::poll_read(source, cx, &mut input_buf))?;
                // (the above will yield to Pending if no more bytes are available.)

                // ... if it didn't change, we're at the end of the stream.
                if *filled_until == input_buf.filled().len() {
                    // End of stream
                    break;
                }
                *filled_until = input_buf.filled().len();
            }

            if *filled_until == 0 {
                // EOF, return without touching the buffer.
                return std::task::Poll::Ready(Ok(()));
            }

            log::debug!("Decrypting block {}", block);

            // Decrypt buffer
            let mut nonce = [0u8; 12];
            nonce[0..8].clone_from_slice(&block.to_le_bytes());
            scratch_space.truncate(*filled_until);
            cipher
                .as_ref()
                .unwrap()
                .decrypt_in_place(Nonce::from_slice(&nonce), b"", scratch_space)
                .expect("decrypt");
            *block += 1;
        }

        let written_to_sink = written_to_sink.get_or_insert(0);

        let amount_to_write =
            std::cmp::min(buf.remaining(), scratch_space.len() - *written_to_sink);

        assert!(buf.remaining() >= amount_to_write);
        buf.put_slice(&scratch_space[*written_to_sink..][..amount_to_write]);
        *written_to_sink += amount_to_write;

        // If everything is written out, advance.
        if *written_to_sink == scratch_space.len() {
            *filled_until = 0;
            self.written_to_sink = None;
        }

        std::task::Poll::Ready(Ok(()))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::RngCore;
    use tokio::io::AsyncReadExt;

    struct MockSource(std::io::Cursor<Vec<u8>>);
    impl MockSource {
        pub fn random_bytes(n: usize) -> Self {
            let mut inner = vec![0u8; n];
            rand::thread_rng().fill_bytes(&mut inner);
            Self(std::io::Cursor::new(inner))
        }

        pub fn bytes(&self) -> Vec<u8> {
            self.0.get_ref().to_owned()
        }
    }

    impl AsyncRead for MockSource {
        fn poll_read(
            mut self: std::pin::Pin<&mut Self>,
            cx: &mut std::task::Context<'_>,
            buf: &mut ReadBuf<'_>,
        ) -> std::task::Poll<std::result::Result<(), std::io::Error>> {
            let source = std::pin::Pin::new(&mut self.0);
            AsyncRead::poll_read(source, cx, buf)
        }
    }

    async fn read_until_end<T: AsyncRead + Unpin>(mut r: T) -> anyhow::Result<Vec<u8>> {
        let mut out = Vec::new();
        let mut buf = [0u8; 256];

        loop {
            let n = r.read(&mut buf).await?;
            out.extend(&buf[..n]);
            if n == 0 {
                break;
            }
        }
        Ok(out)
    }

    #[tokio::test]
    async fn encrypt_several_lengths_and_check() -> anyhow::Result<()> {
        let lengths = [
            10,
            BLOCK_SIZE,
            BLOCK_SIZE - 16,
            BLOCK_SIZE - 16 + 1,
            BLOCK_SIZE - 16 - 1,
            BLOCK_SIZE + 1,
            BLOCK_SIZE - 1,
            2 * BLOCK_SIZE,
            2 * BLOCK_SIZE - 16,
            2 * BLOCK_SIZE + 16,
            2 * BLOCK_SIZE - 1,
            2 * BLOCK_SIZE + 1,
        ];

        for length in &lengths {
            println!("Testing len = {}", length);
            let buf = MockSource::random_bytes(*length);
            let plaintext = buf.bytes();
            let blocks = if plaintext.len() % BLOCK_SIZE == 0 {
                plaintext.len()
            } else {
                BLOCK_SIZE - plaintext.len() % BLOCK_SIZE + plaintext.len()
            } / BLOCK_SIZE;

            let (public, private) = generate_keypair();
            let sealer = BackupStreamSealer::from_source_and_key(buf, &public);
            let out = read_until_end(sealer).await?;
            assert_eq!(out.len(), plaintext.len() + 32 + blocks * 16);

            let opener = BackupStreamOpener::from_source_and_key(
                MockSource(std::io::Cursor::new(out)),
                private,
            );
            let decrypted = read_until_end(opener).await?;

            assert_eq!(decrypted, plaintext);
        }

        Ok(())
    }
}
