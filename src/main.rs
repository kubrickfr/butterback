use anyhow::Result;
use async_compression::tokio::bufread::{ZstdDecoder, ZstdEncoder};
use glacier::GlacierRetrievalTier;
use indicatif::HumanBytes;
use indicatif::{MultiProgress, ProgressBar, ProgressIterator, ProgressStyle};
use std::path::{Path, PathBuf};
use structopt::StructOpt;

use crate::{
    backup::{BackupJobKind, CloudTarget, Inventory, JobDescription},
    btrfs::BtrfsBackupPlanner,
    glacier::GlacierRetrievalParameters,
    util::AsyncReadStreamCounter,
};

pub mod backup;
pub mod btrfs;
pub mod crypto;
pub mod glacier;
mod util;

mod retry;

#[derive(Debug, StructOpt)]
pub enum Opt {
    Backup(BackupCommand),
    Restore(RestoreCommand),
    KeyGen(KeyGenCommand),
    /// Recreate inventory file from the cloud provider.
    ///
    /// For Amazon, expect four hours.
    ScanInventory(InventoryScanCommand),
    /// Pretty-prints the current back-up inventory
    Inventory(ShowInventoryCommand),
}

#[derive(Debug, structopt::StructOpt)]
pub struct RestoreCommand {
    /// The path where to read the inventory.
    #[structopt(short, long, default_value = "inventory.yaml")]
    pub inventory: PathBuf,

    /// The path where to restore the subvolume to.
    #[structopt(short, long)]
    pub subvolume: PathBuf,

    /// The path to the private key that should decrypt this backup
    #[structopt(short, long)]
    pub key: PathBuf,

    /// Name of the AWS region to download from
    #[structopt(short, long)]
    pub region: String,

    /// Name of the Glacier Vault to download from
    #[structopt(short, long)]
    pub vault: String,

    /// Expedited retrieval mode (costly), instead of standard.
    #[structopt(short, long, conflicts_with("bulk"))]
    pub expedited: bool,

    /// Bulk retrieval mode (cheaper), instead of standard.
    #[structopt(short, long, conflicts_with("expedited"))]
    pub bulk: bool,
}

#[derive(Debug, structopt::StructOpt)]
pub struct ShowInventoryCommand {
    /// The path where to store the inventory.
    #[structopt(short, long, default_value = "inventory.yaml")]
    pub inventory: PathBuf,
}

impl ShowInventoryCommand {
    pub async fn run(self) -> anyhow::Result<()> {
        let Self { inventory } = self;
        let inventory = Inventory::load(&inventory)?;

        if inventory.is_empty() {
            println!("No backups in inventory.  Try scanning the inventory with `butterback scan-inventory`");
        }

        let mut total = 0;
        for volume in inventory.volumes() {
            let backups = inventory.backups_for(&volume);
            println!("Backups for volume {}", volume.display());
            let mut subtotal = 0;
            for crate::backup::InventoryEntry { archive, job: _ } in backups {
                let glacier_metadata: Option<glacier::GlacierArchiveDescription> =
                    serde_json::from_str(&archive.metadata).ok();
                if let Some(glacier_metadata) = glacier_metadata {
                    println!(
                        "├── at {} ({})",
                        archive.date,
                        HumanBytes(glacier_metadata.size)
                    );
                    subtotal += glacier_metadata.size;
                }
            }
            println!(
                "└────>  Total accumulated archive size for {}: {} bytes ({})",
                volume.display(),
                subtotal,
                HumanBytes(subtotal)
            );
            total += subtotal;
        }

        println!();
        println!(
            "Total accumulated archive size: {} bytes ({})",
            total,
            HumanBytes(total)
        );

        Ok(())
    }
}

#[derive(Debug, structopt::StructOpt)]
pub struct InventoryScanCommand {
    /// The path where to store the inventory.
    #[structopt(short, long, default_value = "inventory.yaml")]
    pub inventory: PathBuf,

    /// Name of the AWS region containing the vault
    #[structopt(short, long)]
    pub region: String,

    /// Name of the Glacier Vault to scan
    #[structopt(short, long)]
    pub vault: String,
}

impl InventoryScanCommand {
    pub async fn run(self) -> anyhow::Result<()> {
        let Self {
            inventory: inventory_path,
            region,
            vault,
        } = self;
        let client = glacier::GlacierVault::vault(region.parse()?, vault);
        let inventory = client.inventory().await?;
        inventory.write(&inventory_path)?;
        Ok(())
    }
}

#[derive(Debug, structopt::StructOpt)]
pub struct KeyGenCommand {
    /// The path where to store the key material.
    ///
    /// Will generate `path.priv` and `path.pub`, and as such by default generates `butterback.priv` and `butterback.pub`
    #[structopt(short, long, default_value = "butterback")]
    pub path: PathBuf,

    /// Overwrite existing key material.
    #[structopt(short, long)]
    pub force: bool,
}

#[derive(Debug, structopt::StructOpt)]
pub struct BackupCommand {
    /// The path to the subvolume
    #[structopt(short, long)]
    pub subvolume: PathBuf,

    /// The path to the public key that should encrypt this backup
    #[structopt(short, long)]
    pub key: PathBuf,

    /// Name of the AWS region to upload to
    #[structopt(short, long)]
    pub region: String,

    /// Name of the Glacier Vault to upload to
    #[structopt(short, long)]
    pub vault: String,

    /// The path to where `butterback` can keep volume snapshots to compute incremental backups.
    ///
    /// Defaults to `subvolume/.butterback/`
    #[structopt(long)]
    pub snapshots: Option<PathBuf>,

    /// The filepath where `butterback` may store its inventory to compute incremental backups
    ///
    /// Defaults to `inventory.yaml`
    #[structopt(short, long, default_value = "inventory.yaml")]
    pub inventory: PathBuf,

    /// Dry run, only show backup plan.
    #[structopt(long)]
    pub show_plan: bool,
}

impl KeyGenCommand {
    async fn run(self) -> Result<()> {
        let Self { path, force } = self;

        let (pubk, privk) = crypto::generate_keypair();
        let (pub_path, priv_path) = if let Some(ext) = path.extension() {
            let mut pub_ext = ext.to_owned();
            pub_ext.push(".pub");
            let mut priv_ext = ext.to_owned();
            priv_ext.push(".priv");
            (path.with_extension(pub_ext), path.with_extension(priv_ext))
        } else {
            (path.with_extension("pub"), path.with_extension("priv"))
        };

        anyhow::ensure!(
            force || (!pub_path.exists() && !priv_path.exists()),
            "Supply --force to overwrite existing keys."
        );
        pubk.write_to(&pub_path)?;
        privk.write_to(&priv_path)?;

        Ok(())
    }
}

impl BackupCommand {
    async fn run(self) -> Result<()> {
        let Self {
            subvolume,
            key,
            snapshots,
            region,
            vault,
            inventory: inventory_path,
            show_plan,
        } = self;

        let progressbar = MultiProgress::new();

        let snapshots = snapshots.unwrap_or_else(|| subvolume.join(".butterback"));

        let key = crypto::PublicKey::load_from(&key)?;
        let mut inventory = if inventory_path.exists() {
            log::trace!("Inventory path {} exists", inventory_path.display());
            Inventory::load(&inventory_path).expect("readable inventory")
        } else {
            log::warn!(
                "Inventory path {} not found.  Creating empty inventory.",
                inventory_path.display()
            );
            Inventory::default()
        };

        let planner = BtrfsBackupPlanner::for_subvolume(subvolume.clone()).await?;
        let mut volumes = planner.list_subvolumes().await?;
        volumes.exclude(&snapshots);

        let plan = volumes.compute_plan(&inventory)?;
        log::info!("Backup plan: {}", plan);

        if show_plan {
            log::info!("--show-plan supplied, not continuing.");
            // dry run
            return Ok(());
        }

        let (snapshot_map, jobs) = plan.create_snapshots(&snapshots).await?;
        let job_count = jobs.len();
        let jobs_progress = ProgressBar::new(job_count as u64).with_message("Backup jobs");
        let jobs_progress = progressbar.add(jobs_progress);
        jobs_progress.enable_steady_tick(std::time::Duration::from_millis(250));
        jobs_progress.set_style(
            ProgressStyle::default_bar()
                .template("{spinner} [{elapsed_precise}] [{wide_bar}] job {pos}/{len}")?,
        );

        jobs_progress.println(format!("Got {} upload jobs.", job_count));

        let glacier = glacier::GlacierVault::vault(region.parse()?, vault);
        for (i, (meta, strategy)) in jobs.into_iter().enumerate().progress_with(jobs_progress) {
            if i == 0 && meta.usage_exclusive.is_none() {
                log::warn!("Enable qgroups on the btrfs partition to get progress estimates.");
                log::info!("Use `btrfs quota enable /volume/here`.  After enabling, btrfs takes some time scanning the extents.");
            }

            let local_snapshot = &snapshot_map[&meta.subvolume];
            let (kind, read_job, estimated_size) = match strategy {
                btrfs::SubvolumeStrategy::Full => (
                    BackupJobKind::Full,
                    btrfs::BtrfsSubvolumeReadJob::from_snapshot(local_snapshot)?,
                    meta.usage_referenced, // "exclusive" is included in referenced
                ),
                btrfs::SubvolumeStrategy::Incremental(entry) => {
                    let jobkind = BackupJobKind::Incremental {
                        parent: entry.archive.archive_id,
                    };
                    (
                        jobkind,
                        btrfs::BtrfsSubvolumeReadJob::from_delta_snapshots(
                            local_snapshot,
                            Path::new(&entry.job.local_snapshot),
                        )?,
                        meta.usage_exclusive, // if the base was chosen well, this is correct
                    )
                }
                btrfs::SubvolumeStrategy::Delta(base) => {
                    // base is in-tree, backed-up before this job.
                    // Find it in the snapshot_map, then look up the snapshot in the inventory.
                    let entry = inventory.entry_for_snapshot(&snapshot_map[&base]).unwrap();
                    (
                        BackupJobKind::Incremental {
                            parent: entry.archive.archive_id.clone(),
                        },
                        btrfs::BtrfsSubvolumeReadJob::from_delta_snapshots(
                            local_snapshot,
                            Path::new(&entry.job.local_snapshot),
                        )?,
                        meta.usage_exclusive, // if the base is chosen well, this is correct
                    )
                }
                btrfs::SubvolumeStrategy::UpToDate(_entry) => {
                    // XXX in fact, we could also drop this from the snapshots...
                    log::info!(
                        "No changes to {} since last back-up, skipping.",
                        meta.subvolume.display()
                    );
                    continue;
                }
            };

            let pb = if let Some(est) = estimated_size {
                let pb = ProgressBar::new(est)
                    .with_style(ProgressStyle::default_bar()
                                .template("{spinner:.green} [{elapsed_precise}] {msg} [{wide_bar:.cyan/blue}] {bytes}/{total_bytes} (est) ({bytes_per_sec}, {eta})")?
                                .progress_chars("#>-"))
                    .with_message(format!("Job {}: {}", i + 1, meta.subvolume.display()));
                pb.enable_steady_tick(std::time::Duration::from_millis(250));
                Some(progressbar.add(pb))
            } else {
                None
            };

            let description = JobDescription {
                volume: meta.subvolume.display().to_string(),
                kind,
                generation: meta.generation,
                local_snapshot: snapshot_map
                    .get(&meta.subvolume)
                    .expect("snapshot created for every planned volume")
                    .display()
                    .to_string(),
                uuid: meta.uuid,
                subvolumes: if meta.subvolume == subvolume {
                    snapshot_map
                        .keys()
                        .map(|x| x.display().to_string())
                        .collect()
                } else {
                    vec![]
                },
            };

            let line = match &description.kind {
                backup::BackupJobKind::Incremental { ref parent } => {
                    format!(
                        "Job {}/{} is an incremental backup, on top of archive {}.",
                        i + 1,
                        job_count,
                        parent
                    )
                }
                backup::BackupJobKind::Full => {
                    format!("Job {}/{} is a full backup.", i + 1, job_count)
                }
            };
            if let Some(pb) = pb.as_ref() {
                pb.println(line);
            } else {
                log::info!("{}", line);
            }

            let compressed_stream = ZstdEncoder::new(tokio::io::BufReader::new(
                AsyncReadStreamCounter::new(read_job),
            ));
            let mut encrypted_stream =
                crypto::BackupStreamSealer::from_source_and_key(compressed_stream, &key);

            let mut upload_job = glacier.initiate_upload_job(description).await?;
            // XXX run cleanup on error

            let upload_result = async {
                use tokio::io::AsyncReadExt;
                let mut buf = [0u8; 1024];
                loop {
                    let read = encrypted_stream.read(&mut buf).await?;
                    if read == 0 {
                        // EOF
                        break;
                    }
                    upload_job.write(&buf[..read]).await?;

                    if let Some(pb) = pb.as_ref() {
                        pb.set_position(
                            encrypted_stream.get_ref().get_ref().get_ref().count() as u64
                        );
                    }
                }
                Ok(())
            }
            .await;

            if let Err(e) = upload_result {
                println!("Cleaning up after error ({})", e);
                upload_job.clean_up().await?;
                return Err(e);
            }

            if let Some(pb) = pb.as_ref() {
                pb.set_length(encrypted_stream.get_ref().get_ref().get_ref().count() as u64);
                pb.finish_with_message("Finalizing archive.");
            } else {
                println!("Finalizing archive.");
            }
            let desc = upload_job.finalize().await?;

            // Update inventory file
            inventory.push(desc)?;
            inventory.write(&inventory_path)?;
        }

        Ok(())
    }
}

impl RestoreCommand {
    async fn run(self) -> Result<()> {
        let Self {
            subvolume: subvolume_path,
            key,
            region,
            vault,
            inventory: inventory_path,
            expedited,
            bulk,
        } = self;

        let key = crypto::PrivateKey::load_from(&key)?;

        // Read inventory file
        let inventory = Inventory::load(&inventory_path)?;

        let glacier = glacier::GlacierVault::vault(region.parse()?, vault);
        let mut entries: Vec<_> = inventory.backups_for(&subvolume_path).collect();
        // XXX this is probably not needed
        entries.sort_by_key(|b| b.archive.date);
        let entry = entries
            .last()
            .ok_or_else(|| anyhow::format_err!("No backups for {}", subvolume_path.display()))?;
        println!(
            "Most recent back-up for {} is from {}",
            subvolume_path.display(),
            entry.archive.date
        );

        let mut subvolume = btrfs::BtrfsSubvolumeWriteJob::write_to_subvolume(&subvolume_path)?;

        let job = match glacier.continue_job(&entry.archive).await? {
            Some(job) => {
                log::info!("Continuing existing retrieval job");
                job
            }
            None => {
                glacier
                    .initiate_retrieval_job(
                        &entry.archive,
                        GlacierRetrievalParameters {
                            tier: if expedited {
                                GlacierRetrievalTier::Expedited
                            } else if bulk {
                                GlacierRetrievalTier::Bulk
                            } else {
                                GlacierRetrievalTier::default()
                            },
                        },
                    )
                    .await?
            }
        };

        let stream = tokio_util::io::StreamReader::new(job.into_blocks());
        let opened_stream = crypto::BackupStreamOpener::from_source_and_key(stream, key);
        // 2 MB buffered reading.  This way, if `btrfs` can follow network speed, there's
        // always something downloading.
        let opened_stream = tokio::io::BufReader::with_capacity(2 * 1024 * 1024, opened_stream);
        let decompressed_stream = ZstdDecoder::new(opened_stream);
        let mut decompressed_stream = tokio::io::BufReader::new(decompressed_stream);
        tokio::io::copy_buf(&mut decompressed_stream, &mut subvolume).await?;

        subvolume.close().await?;

        log::info!("Recovered {}", subvolume_path.display());

        Ok(())
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    dotenv::dotenv().ok();
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("butterback=info"))
        .init();
    let opt = Opt::from_args();
    match opt {
        Opt::Backup(cmd) => cmd.run().await?,
        Opt::Restore(cmd) => cmd.run().await?,
        Opt::KeyGen(cmd) => cmd.run().await?,
        Opt::ScanInventory(cmd) => cmd.run().await?,
        Opt::Inventory(cmd) => cmd.run().await?,
    }
    Ok(())
}
